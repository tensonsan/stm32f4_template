
#ifndef _SYS_INIT_H_
#define _SYS_INIT_H_

#include <stdint.h>

int32_t sys_clock_init(void);
void sys_clock_print(void);

#endif
