
#include "led.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

void led_init(void)
{
    __HAL_RCC_GPIOD_CLK_ENABLE();

    GPIO_InitTypeDef init;

    init.Pin = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
    init.Mode = GPIO_MODE_OUTPUT_PP;
    init.Pull = GPIO_NOPULL;
    init.Speed = GPIO_SPEED_FREQ_LOW;

    HAL_GPIO_Init(GPIOD, &init);
}

void led_green(bool state)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void led_blue(bool state)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void led_red(bool state)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void led_orange(bool state)
{
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}