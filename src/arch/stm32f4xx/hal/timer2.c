
#include "timer2.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

#define TIM2_CLOCK  1000000

static TIM_HandleTypeDef tim2;

int32_t timer2_init(void)
{
    uint32_t tim_clk = HAL_RCC_GetPCLK1Freq();

    uint32_t latency = 0;
    RCC_ClkInitTypeDef clk_cfg;

    HAL_RCC_GetClockConfig(&clk_cfg, &latency);
    if (1 == clk_cfg.APB1CLKDivider)
    {
        tim_clk *= 2;
    }

    tim2.Instance = TIM2;
    tim2.Init.Prescaler = (tim_clk / TIM2_CLOCK) - 1;
    tim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    tim2.Init.Period = 0xFFFFFFFF;
    tim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    tim2.Init.RepetitionCounter = 0;

    if (HAL_OK != HAL_TIM_Base_Init(&tim2))
    {
        return -1;
    }

    if (HAL_OK != HAL_TIM_Base_Start(&tim2))
    {
        return -1;
    }

    return 0;
}

uint32_t timer2_read(void)
{
    return __HAL_TIM_GET_COUNTER(&tim2);
}
