
#ifndef _TIMER2_H_
#define _TIMER2_H_

#include <stdint.h>

int32_t timer2_init(void);
uint32_t timer2_read(void);

#endif
