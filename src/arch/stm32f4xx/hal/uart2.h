
#ifndef _UART2_H_
#define _UART2_H_

#include <stdint.h>

int32_t uart2_init(uint32_t baudrate);
int32_t uart2_write_polling(uint8_t *data, int32_t size);
int32_t uart2_write(uint8_t *data, int32_t size, uint32_t timeout);
int32_t uart2_read(uint8_t *data, int32_t size, uint32_t timeout);

#endif
