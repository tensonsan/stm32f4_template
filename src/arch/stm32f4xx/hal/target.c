
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
    if (TIM2 == htim->Instance)
    {
        __HAL_RCC_TIM2_CLK_ENABLE();
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
    if (USART2 == huart->Instance)
    {
        __HAL_RCC_USART2_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();

        // PA2 USART2 TX - FTDI yellow
        // PA3 USART2 RX - FTDI orange

        GPIO_InitTypeDef init;

        init.Pin = GPIO_PIN_2 | GPIO_PIN_3;
        init.Mode = GPIO_MODE_AF_PP;
        init.Pull = GPIO_NOPULL;
        init.Speed = GPIO_SPEED_FREQ_HIGH;
        init.Alternate = GPIO_AF7_USART2;

        HAL_GPIO_Init(GPIOA, &init);
    }
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    if (SPI2 == hspi->Instance)
    {
        __HAL_RCC_SPI2_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();

        GPIO_InitTypeDef init;

        // PB13 SPI2 SCK
        // PB14 SPI2 MISO
        // PB15 SPI2 MOSI

        init.Alternate = GPIO_AF5_SPI2;
        init.Mode = GPIO_MODE_AF_PP;
        init.Pull = GPIO_NOPULL;
        init.Speed = GPIO_SPEED_FREQ_HIGH;
        init.Pin = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;

        HAL_GPIO_Init(GPIOB, &init);

        // PB12 SPI2 NSS

        init.Mode = GPIO_MODE_OUTPUT_PP;
        init.Pin = GPIO_PIN_12;

        HAL_GPIO_Init(GPIOB, &init);

        // De-assert CS
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
    }
}
