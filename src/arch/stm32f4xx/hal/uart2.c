
#include "cmsis_os.h"
#include "queue.h"
#include "uart2.h"
#include "stm32f4xx_hal.h"

#define UART_RX_QUEUE_LEN   128
#define UART_TX_QUEUE_LEN   128

static UART_HandleTypeDef usart2;

osMessageQDef(_rx_queue, UART_RX_QUEUE_LEN, uint8_t);
static osMessageQId rx_queue;

osMessageQDef(_tx_queue, UART_TX_QUEUE_LEN, uint8_t);
static osMessageQId tx_queue;

void USART2_IRQHandler(void)
{
    uint8_t ch = 0;
    uint32_t sr = READ_REG(usart2.Instance->SR);
    uint32_t cr1 = READ_REG(usart2.Instance->CR1);

    // RX interrupt
    if ((sr & USART_SR_RXNE) && (cr1 & USART_IT_RXNE))
    {
        ch = READ_REG(usart2.Instance->DR);
        osMessagePut(rx_queue, ch, 0);

        __HAL_UART_CLEAR_FLAG(&usart2, USART_SR_RXNE);
    }

    // Overrun interrupt
    if ((sr & USART_SR_ORE) && (cr1 & USART_IT_RXNE))
    {
        ch = READ_REG(usart2.Instance->DR);
    }

    // TX interrupt
    if ((sr & USART_SR_TXE) && (cr1 & USART_IT_TXE))
    {
        __HAL_UART_CLEAR_FLAG(&usart2, USART_SR_TXE);

        osEvent evt =  osMessageGet(tx_queue, 0);
        if (osEventMessage == evt.status)
        {
            WRITE_REG(usart2.Instance->DR, evt.value.v);
        }
        else
        {
            __HAL_UART_DISABLE_IT(&usart2, UART_IT_TXE);
        }
    }
}

int32_t uart2_init(uint32_t baudrate)
{
    // Print & receive characters immediately
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);

    tx_queue = osMessageCreate(osMessageQ(_tx_queue), 0);
    rx_queue = osMessageCreate(osMessageQ(_tx_queue), 0);

    usart2.Instance = USART2;
    usart2.Init.BaudRate = baudrate;
    usart2.Init.WordLength = USART_WORDLENGTH_8B;
    usart2.Init.StopBits = USART_STOPBITS_1;
    usart2.Init.Parity = USART_PARITY_NONE;
    usart2.Init.Mode = USART_MODE_TX_RX;
    usart2.Init.OverSampling = UART_OVERSAMPLING_16;

    if (HAL_OK != HAL_UART_Init(&usart2))
    {
        return -1;
    }

    __HAL_UART_DISABLE_IT(&usart2, UART_IT_TXE);
    __HAL_UART_ENABLE_IT(&usart2, UART_IT_RXNE);

    HAL_NVIC_SetPriority(USART2_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY+2, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);

    return 0;
}

int32_t uart2_write(uint8_t *data, int32_t size, uint32_t timeout)
{ 
    int32_t i;

    for (i=0; i < size; ++i)
    {
        osMessagePut(tx_queue, *data++, timeout);
        __HAL_UART_ENABLE_IT(&usart2, UART_IT_TXE);
    }
    return i;
}

int32_t uart2_write_polling(uint8_t *data, int32_t size)
{
    int32_t i;

    for (i=0; i < size; ++i)
    {
        WRITE_REG(usart2.Instance->DR, data[i]);
        while (1)
        {
            uint32_t sr = READ_REG(usart2.Instance->SR);
            if (sr & USART_SR_TXE)
            {
                break;
            }
        }
    }
    return i;
}

int32_t uart2_read(uint8_t *data, int32_t size, uint32_t timeout)
{
    int32_t i;

    for (i=0; i < size; ++i)
    {
        osEvent evt = osMessageGet(rx_queue, timeout);
        if (osEventMessage != evt.status)
        {
            break;
        }
        *data++ = (uint8_t)evt.value.v;
    }
    return i;
}