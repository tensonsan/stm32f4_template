
#ifndef _LED_H_
#define _LED_H_

#include <stdbool.h>

void led_init(void);
void led_green(bool state);
void led_blue(bool state);
void led_red(bool state);
void led_orange(bool state);

#endif
