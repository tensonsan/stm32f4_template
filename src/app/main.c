
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "sys_init.h"
#include "led.h"
#include "uart2.h"
#include "cli.h"
#include "stdlib.h"
#include "cmsis_os.h"
#include "timer2.h"

#define CLI_BAUDRATE 115200

void cli_task(void const *arg);
osThreadDef(cli, cli_task, osPriorityNormal, 1, 1024);

static int32_t CLI_JOB(led)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(help)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(task_list)(int32_t argc, char *argv[]);
static int32_t CLI_JOB(clock_list)(int32_t argc, char *argv[]);

static cli_handle_t cli_handle;

static const cli_cmd_t cli_table[] =
{
    CLI_CMD(led, "Turns on/off the blue LED. Syntax: led <1|0>"),
    CLI_CMD(task_list, "Displays the list of tasks, their states and stacks."),
    CLI_CMD(clock_list, "Display the MCU clock values."),
    CLI_CMD(help, "Displays help."),
    CLI_CMD_END
};

static int32_t CLI_JOB(task_list)(int32_t argc, char *argv[])
{
    char tmp[256];

    vTaskList(tmp);

    printf("\n\r%s", tmp);

    return 0;
}

static int32_t CLI_JOB(led)(int32_t argc, char *argv[])
{
    if (2 == argc)
    {
        led_blue((bool)atoi(argv[1]));

        return 0;
    }

    return -1;
}

static int32_t CLI_JOB(help)(int32_t argc, char *argv[])
{
    cli_print_help(&cli_handle);

    return 0;
}

static int32_t CLI_JOB(clock_list)(int32_t argc, char *argv[])
{
    sys_clock_print();

    return 0;
}

void cli_task(void const *arg)
{
    int32_t rv = uart2_init(CLI_BAUDRATE);
    if (rv)
    {
        led_red(true);
    }

    cli_init(&cli_handle, cli_table);

    while (1)
    {
        uint8_t ch = getchar();
        cli_process(&cli_handle, (char)ch);
    }
}

int main(void)
{
    led_init();

    int32_t rv = sys_clock_init();
    if (rv)
    {
        led_red(true);
    }

    rv = timer2_init();
    if (rv)
    {
        led_red(true);
    }

    osThreadCreate(osThread(cli), NULL);

    osKernelStart();

    while (1)
    {
    }
}

void vApplicationAssert(void)
{
    led_red(true);
    printf("E: app assert\r\n");

    while(1)
    {
    }
}

void vApplicationStackOverflowHook(void)
{
    led_red(true);
    printf("E: stack overflow\r\n");

    while (1)
    {
    }
}

void vApplicationIdleHook(void)
{
    // TODO: low power mode
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{ 
    led_red(true);
    printf("E: assert\r\n");

    while (1)
    {
    }
}
#endif
