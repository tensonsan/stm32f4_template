
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Command Line Interface
 *
 **********************************************************************/

 /**
  * @addtogroup CLI
  * @{
  */

#ifndef _CLI_CONFIG_H_
#define _CLI_CONFIG_H_

#include <stdio.h>
#include <assert.h>

/// Maximal length of a CLI command
#define CLI_BUFFER_LEN  32

/// Maximal number of CLI command arguments
#define CLI_ARGS_MAX    5

/// CLI prompt string
#define CLI_PROMPT  ">"

/// CLI error strings
//#ifdef USE_TESTS
//#define CLI_ERR_CMD_UNKNOWN     "E: cmd unknown"
//#define CLI_ERR_CMD_ARGUMENTS   "E: cmd arguments"
//#define CLI_ERR_CMD_LENGTH      "E: cmd length"
//#else
#define CLI_ERR_CMD_UNKNOWN     "\e[31mE: cmd unknown\e[39m"
#define CLI_ERR_CMD_ARGUMENTS   "\e[31mE: cmd arguments\e[39m"
#define CLI_ERR_CMD_LENGTH      "\e[31mE: cmd length\e[39m"
//#endif

/// CLI key mappings
#define CLI_CH_ENTER        0x0d
#define CLI_CH_BACKSPACE    0x08
#define CLI_CH_TAB          0x09
#define CLI_KEY_UP          "\e[A"
#define CLI_KEY_DOWN        "\e[B"
#define CLI_KEY_RIGHT       "\e[C"
#define CLI_KEY_LEFT        "\e[D"

/**
 * Uncomment to allow the CLI command help strings.
 * @note Using this define will increase the CLI code footprint.
 */
#define CLI_USE_HELP 1

/**
 * Uncomment to allow the CLI command history (navigation via up/down keys).
 * The maximal number of history entries is defined by \p CLI_HISTORY_MAX.
 * @note Using this define will increase the CLI code footprint.
 */
#define CLI_USE_HISTORY 1

/// Maximal number of previous CLI commands to keep in a history buffer
#ifdef CLI_USE_HISTORY
#define CLI_HISTORY_MAX 3
#else
#define CLI_HISTORY_MAX 1
#endif

/**
 * Uncomment to allow the CLI command autocompletion (navigation via tab key).
 * @note Using this define will increase the CLI code footprint.
 */
#define CLI_USE_AUTOCOMPLETE    1

/// Set the printf function for CLI printouts
#ifdef USE_TESTS
#include "main.h"
#define CLI_PRINTF(f, ...)  cli_test_printf((f), ##__VA_ARGS__)
#else
#define CLI_PRINTF(f, ...)  printf((f), ##__VA_ARGS__)
#endif

/// Set the assert function for debugging
#define CLI_ASSERT(expr)    assert(expr)

#endif

/**
 * @}
 */
