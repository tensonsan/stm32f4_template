
/**********************************************************************
 *
 * @file
 * @author  G.K.
 * @version 1.0
 * @brief   Command Line Interface
 *
 **********************************************************************/

/**
 * @addtogroup CLI
 * @{
 */

#include <string.h>
#include "cli.h"

#ifdef CLI_USE_HISTORY
static void cli_history_prev(void *arg);
static void cli_history_next(void *arg);
#endif
static void cli_cursor_left(void *arg);
static void cli_cursor_right(void *arg);

/// Escape sequence table
const cli_es_t cli_es_table[] = 
{
#ifdef CLI_USE_HISTORY
    {CLI_KEY_UP, cli_history_prev},
    {CLI_KEY_DOWN, cli_history_next},
#endif
    {CLI_KEY_RIGHT, cli_cursor_right},
    {CLI_KEY_LEFT, cli_cursor_left},
    {NULL, NULL}
};

/**
 * @brief Moves cursor left
 * 
 * @param[in] arg CLI handle
 */
static void cli_cursor_left(void *arg)
{
    cli_handle_t *h = (cli_handle_t *)arg;

    if (h->position > 0)
    {
        h->position--;
        CLI_PRINTF(CLI_KEY_LEFT);
    }
}

/**
 * @brief Moves cursor right
 * 
 * @param[in] arg CLI handle
 */
static void cli_cursor_right(void *arg)
{
    cli_handle_t *h = (cli_handle_t *)arg;

    if (h->position < h->length)
    {
        h->position++;
        CLI_PRINTF(CLI_KEY_RIGHT);
    }
}

/**
 * @brief Deletes a character from the CLI buffer
 * 
 * @param[in] h CLI handle
 */
static void cli_char_delete(cli_handle_t *h)
{
    // No more characters?
    if (h->length <= 0)
    {
        return;
    }

    if (h->position == h->length)
    {
        // Remove the end character
        h->buffer[h->length--] = '\0';
        h->position = h->length;

        CLI_PRINTF("\b \b");
    }
    else
    {
        // Remove the middle/start character
        h->length--;

        if (h->position > 0)
        {
            h->position--;
        }

        for (int32_t i=h->position; i < h->length; ++i)
        {
            h->buffer[i] = h->buffer[i + 1];
        }

        h->buffer[h->length] = '\0';

        CLI_PRINTF("\r%s%s ", CLI_PROMPT, h->buffer);
        for (int32_t i=h->position; i < (h->length + 1); ++i)
        {
            CLI_PRINTF("\b");
        }
    }
}

/**
 * @brief Inserts a character to the CLI buffer
 * 
 * @param[in] h CLI handle
 * @param[in] ch Input character
 */
static void cli_char_insert(cli_handle_t *h, char ch)
{
    if (h->length >= CLI_BUFFER_LEN)
    {
        // Command too long
        CLI_PRINTF("\r\n%s\r\n%s", CLI_ERR_CMD_LENGTH, CLI_PROMPT);

        h->length = 0;
        h->position = h->length;
    }

    if (h->position == h->length)
    {
        // Append a character
        CLI_PRINTF("%c", ch);

        h->buffer[h->length++] = ch;
        h->position = h->length;
    }
    else
    {
        // Insert a character
        h->length++;
        for (int32_t i=h->length; i > h->position; --i)
        {
            h->buffer[i] = h->buffer[i-1];
        }
        h->buffer[h->position++] = ch;
        h->buffer[h->length] = '\0';

        CLI_PRINTF("\r%s%s", CLI_PROMPT, h->buffer);
        for (int32_t i=h->position; i < h->length; ++i)
        {
            CLI_PRINTF("\b");
        }
    }
}

/**
 * @brief Processes the CLI command
 * 
 * @param[in] h CLI handle
 */
static void cli_handle_command(cli_handle_t *h)
{
    h->argc = 0;

    char *s = h->buffer;
    h->argv[h->argc++] = s;

    // Tokanize the input buffer
    while (*s)
    {
        if (' ' == *s)
        {
            *s = '\0';
            if (h->argc >= CLI_ARGS_MAX)
            {
                // Too many arguments
                CLI_PRINTF("\r\n%s\r\n%s", CLI_ERR_CMD_ARGUMENTS, CLI_PROMPT);
                return;
            }
            h->argv[h->argc++] = s + 1;
        }
        s++;
    }

    // Check for a command match and run its job
    cli_cmd_t *item = h->table;
    while(item->cmd)
    {
        if (!strcmp(item->cmd, h->argv[0]))
        {
            int32_t rv = item->job(h->argc, h->argv);
            CLI_PRINTF("\r\ncmd: %d\r\n%s", (int)rv, CLI_PROMPT);
            return;
        }
        item++;
    }

    // Command not found
    CLI_PRINTF("\r\n%s\r\n%s", CLI_ERR_CMD_UNKNOWN, CLI_PROMPT);
}

#ifdef CLI_USE_AUTOCOMPLETE
/**
 * @brief Handles a CLI command automatic completion
 * 
 * @param[in] h CLI handle
 */
static void cli_handle_autocomplete(cli_handle_t *h)
{
    int32_t num_hits = 0, i = 0, index;

    cli_cmd_t *item = h->table;
    while (item->cmd)
    {
        // Command match?
        if (!memcmp(item->cmd, h->buffer, h->length))
        {
            num_hits++;
            index = i;
        }
        i++;
        item++;
    }

    if (0 == num_hits)
    {
        // No matches, nothing to do
    }
    else if (1 == num_hits)
    {
        // Single match, use the command
        CLI_PRINTF("%s", h->table[index].cmd + h->length);

        strcpy(h->buffer, h->table[index].cmd);
        h->length = strlen(h->buffer);
        h->position = h->length;
    }
    else
    {
        // Multiple matches, list all corresponding commands
        item = h->table;
        while (item->cmd)
        {
            if (!memcmp(item->cmd, h->buffer, h->length))
            {
                CLI_PRINTF("\r\n%s", item->cmd);
            }
            item++;
        }
        h->buffer[h->length] = '\0';

        CLI_PRINTF("\r\n%s%s", CLI_PROMPT, h->buffer);
    }
}
#endif

#ifdef CLI_USE_HISTORY
/**
 * @brief Initializes the CLI history
 * 
 * @note Must be called once prior any cli_history_ function
 * 
 * @param[in] h CLI handle
 */
static void cli_history_init(cli_handle_t *h)
{
    h->history.count = 0;
    h->history.index = 0;
    h->history.wr = 0;
    h->history.rd = 0;
}

/**
 * @brief Pushes a new command to the CLI history
 * 
 * @param[in] h CLI handle
 */
static void cli_history_push(cli_handle_t *h)
{
    // Enqueue a command to the history buffer (circular)
    strcpy(&(h->history.buffer[h->history.wr++][0]), h->buffer);
    if (h->history.wr >= CLI_HISTORY_MAX)
    {
        h->history.wr = 0;
    }

    h->history.rd = h->history.wr;
    h->history.index = 0;

    // Increase the number of history entries
    if (h->history.count < CLI_HISTORY_MAX)
    {
        h->history.count++;
    }
}

/**
 * @brief Regresses to the previous history entry
 * 
 * @param[in] arg CLI handle
 */
static void cli_history_prev(void *arg)
{
    cli_handle_t *h = (cli_handle_t *)arg;

    if (h->history.count <= 0)
    {
        // No command history
        return;
    }

    if (h->history.index < h->history.count)
    {
        // Previous history entry active
        h->history.rd--;
        if (h->history.rd < 0)
        {
            h->history.rd += h->history.count;
        }
        h->history.index++;

        strcpy(h->buffer, &(h->history.buffer[h->history.rd][0]));
        h->length = strlen(h->buffer);
        h->position = h->length;

        CLI_PRINTF("\r\n%s%s", CLI_PROMPT, h->buffer);
    }
}

/**
 * @brief Advances to the next history entry
 * 
 * @param[in] arg CLI handle
 */
static void cli_history_next(void *arg)
{
    cli_handle_t *h = (cli_handle_t *)arg;

    if (h->history.count <= 0)
    {
        // No command history
        return;
    }

    if (1 == h->history.index)
    {
        // Leaving history, going to the command prompt
        h->history.rd++;
        if (h->history.rd >= h->history.count)
        {
            h->history.rd = 0;
        }
        h->history.index--;

        h->buffer[0] = '\0';
        h->length = 0;
        h->position = h->length;

        CLI_PRINTF("\r\n%s", CLI_PROMPT);
    }
    else if (h->history.index > 1)
    {
        // Next history entry active
        h->history.rd++;
        if (h->history.rd >= h->history.count)
        {
            h->history.rd = 0;
        }
        h->history.index--;

        strcpy(h->buffer, &(h->history.buffer[h->history.rd][0]));
        h->length = strlen(h->buffer);
        h->position = h->length;

        CLI_PRINTF("\r\n%s%s", CLI_PROMPT, h->buffer);
    }
    else
    {
        // Left history, nothing to do
    }
}
#endif

/**
 * @brief Initializes escape sequence handler
 * 
 * @note Must be called once prior \p cli_escape_sequence_handle
 * 
 * @param[in] h CLI handle
 */
static void cli_escape_sequence_init(cli_handle_t *h)
{
    h->escape.max_len = 0;
    h->escape.length = 0;
    h->escape.table = (cli_es_t *)cli_es_table;

    // Find the maximal length of a known escape sequence
    cli_es_t *item = h->escape.table;
    while (item->sequence)
    {
        int32_t len = strlen(item->sequence);
        if (len > h->escape.max_len)
        {
            h->escape.max_len = len;
        }
        item++;
    }
}

/**
 * @brief Handles the escape sequence
 * 
 * @param[in] h CLI handle
 * @param[in] ch Input character
 * 
 * @return 1 if escape sequence in progress, 0 otherwise
 */
static int32_t cli_escape_sequence_handle(cli_handle_t *h, char ch)
{
    int32_t rv = 0;

    // Escape sequence found?
    if (h->escape.length > 0)
    {
        if (h->escape.length >= h->escape.max_len)
        {
            // Escape sequence too long, restart the parser
            h->escape.length = 0;
        }
        else
        {
            // Store a new escape sequence character
            h->escape.buffer[h->escape.length++] = ch;
            h->escape.buffer[h->escape.length] = '\0';

            // Check for an escape sequence match and run its job
            cli_es_t *item = h->escape.table;
            while (item->sequence)
            {
                if (!strcmp(h->escape.buffer, item->sequence))
                {
                    item->job(h);
                    h->escape.length = 0;
                    break;
                }
                item++;
            }
            rv = 1;
        }
    }
    else
    {
        // Start of an escape sequence?
        if ('\e' == ch)
        {
            h->escape.buffer[h->escape.length++] = ch;
            rv = 1;
        }
    }
    return rv;
}

/**
 * @brief Initializes CLI
 * 
 * @note Must be called once prior \p cli_process
 * 
 * @param[in] h CLI handle
 * @param[in] table CLI command table 
 */
void cli_init(cli_handle_t *h, const cli_cmd_t *table)
{
    CLI_ASSERT(h);
    CLI_ASSERT(table);

    h->table = (cli_cmd_t *)table;
    h->length = 0;
    h->position = 0;
    h->argc = 0;

    cli_escape_sequence_init(h);

#ifdef CLI_USE_HISTORY
    cli_history_init(h);
#endif

    CLI_PRINTF("\r\n%s", CLI_PROMPT);
}

/**
 * @brief Processes CLI
 * 
 * @note Must be called for every input character.
 * 
 * @param[in] h CLI handle
 * @param[in] ch Input character 
 */
void cli_process(cli_handle_t *h, char ch)
{
    CLI_ASSERT(h);

    int32_t rv = cli_escape_sequence_handle(h, ch);
    if (rv)
    {
        return;
    }

    if (CLI_CH_ENTER == ch)
    {
        // Only enter pressed?
        if (h->length > 0)
        {
            h->buffer[h->length] = '\0';

#ifdef CLI_USE_HISTORY
            cli_history_push(h);
#endif
            cli_handle_command(h);

            h->length = 0;
            h->position = h->length;
        }
        else
        {
            CLI_PRINTF("\r\n%s", CLI_PROMPT);
        }
    }
    else if (CLI_CH_BACKSPACE == ch)
    {
        cli_char_delete(h);
    }
    else if (CLI_CH_TAB == ch)
    {
#ifdef CLI_USE_AUTOCOMPLETE
        cli_handle_autocomplete(h);
#endif
    }
    else if ((ch >= 0x20) && (ch <= 0x7E))
    {
        cli_char_insert(h, ch);
    }
    else
    {
        // Skip processing other characters
    }
}

/**
 * @brief Prints help for all CLI commands
 * 
 * @param[in] h CLI handle
 */
void cli_print_help(cli_handle_t *h)
{
    cli_cmd_t *item = h->table;
    while (item->cmd)
    {
#ifdef CLI_USE_HELP
        CLI_PRINTF("\r\n%s %s", item->cmd, item->help);
#else
        CLI_PRINTF("\r\n%s", item->cmd);
#endif
        item++;
    }
}

/**
 * @}
 */
