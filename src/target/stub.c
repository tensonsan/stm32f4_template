
#include <stdio.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include "uart2.h"
#include "cmsis_os.h"

#undef errno
extern int errno;
extern int _end;

caddr_t _sbrk(int incr)
{
    static unsigned char *heap = NULL;
    unsigned char *prev_heap;

    if (heap == NULL)
    {
        heap = (unsigned char *)&_end;
    }
    prev_heap = heap;

    heap += incr;

    return (caddr_t) prev_heap;
}

int _close(int file)
{
    return -1;
}

int _fstat(int file, struct stat *st)
{
    st->st_mode = S_IFCHR;

    return 0;
}

int _isatty(int file)
{
    return 1;
}

int _lseek(int file, int ptr, int dir)
{
    return 0;
}

int _read(int file, char *ptr, int len)
{
    return uart2_read((uint8_t *)ptr, 1, osWaitForever);
}

int _write(int file, char *ptr, int len)
{
    uart2_write((uint8_t *)ptr, len, (uint32_t)len);

    return len;
}

int _getpid(void)
{
    return 1;
}

int _kill(int pid, int sig)
{
    errno = EINVAL;

    return -1;
}

void _exit (int status)
{
    _kill(status, -1);

    while (1) {}
}