## VSCode gcc makefile template project for the STM32F4Discovery board

### Usage:

You need the [VSCode](https://code.visualstudio.com/) installed with [Cortex-Debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug) plugin.

Compile the library into an ELF executable.
```Makefile
make
```

Press F5 to program the STM32F4Discovery and start debugging via the integrated STLink debugger.

Connect the STM32F4Discovery to the PC via serial terminal (e.g. minicom) using a 5V FTDI cable:

[STM32F4Discovery](https://www.st.com/en/evaluation-tools/stm32f4discovery.html) | [TTL-232R-5V](https://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf) |
-------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
PA2                                                                              | yellow                                                                                             |
PA3                                                                              | orange                                                                                             |
GND                                                                              | black                                                                                              |

Terminal settings are:

* Baudrate: 115200
* Databits: 8
* Parity: None
* Stopbits: 1

Once in terminal type "help" to get a list of available CLI commands.

To used colored outputs (e.g. in case of CLI errors) in Minicom:
```Bash
minicom -D /dev/ttyUSB0 -c on
```
